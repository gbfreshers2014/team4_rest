/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.rest_api.test.helper;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.DateTimeAF;
import org.skife.jdbi.v2.tweak.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.iohelpers.IOGBUtils;
import com.gwynniebee.test.helper.liquibase.LiquibaseHelper;

/**
 * Liquibase operation class.
 * @author sumit
 */

public class LiquibaseOperations {

    public static final Logger LOG = LoggerFactory.getLogger(LiquibaseOperations.class);

    private static LiquibaseHelper liquiHelper;

    static {
        try {
            initLiquibaseHelper();
        } catch (Exception e) {
            LOG.error("Unable to initialize Liquibase");
        }
    }

    private static void initLiquibaseHelper() throws Exception {

        Properties props = IOGBUtils.readPropertiesFromFile(new File("configuration/local/emp_prop.properties"));
        String driverName = props.getProperty("gwynniebee_users.driver");
        String mysqlUrl = props.getProperty("gwynniebee_users.url");
        String mysqlUsername = props.getProperty("gwynniebee_users.username");
        String mysqlPassword = props.getProperty("gwynniebee_users.password");
        String changelogFile = "src/main/liquibase/employee-database-service/changelog.xml";

        // Load the JDBC driver
        Class.forName(driverName);

        liquiHelper =
                new LiquibaseHelper(("--driver=" + driverName + " --changeLogFile=" + changelogFile + " --url=" + mysqlUrl + " --username="
                        + mysqlUsername + " --password=" + mysqlPassword + " update").split("\\s"));

        // NOTE: this would only be restarted once as it is static to class
        liquiHelper.killAllConnectionsOfMySQLServer(null);
    }

    public static Connection createConnection() throws SQLException {
        return liquiHelper.createConnection();
    }

    public static void createDatabase() throws Exception {
        liquiHelper.createDatabase();
    }

    public static int createSchemaThroughChangeSet() throws Exception {
        return liquiHelper.createSchemaThroughChangeSet();
    }

    public static void dropDatabase() throws Exception {
        liquiHelper.dropDatabase();
    }

    public static void completeLiquibaseReset() throws Exception {
        dropDatabase();
        createDatabase();
        createSchemaThroughChangeSet();
    }

    public static void cleanTables() throws Exception {
        // clear all data for each test
        liquiHelper.runSqlStatement("DELETE FROM attendance_register");
        liquiHelper.runSqlStatement("DELETE FROM attendance_task");
        liquiHelper.runSqlStatement("DELETE FROM employee_communication_details");
        liquiHelper.runSqlStatement("DELETE FROM employee_family_details");
        liquiHelper.runSqlStatement("DELETE FROM employee_address_details");
        liquiHelper.runSqlStatement("DELETE FROM login_credentials");
        liquiHelper.runSqlStatement("DELETE FROM employee_personal_details");
    }

    public static DBI getDBI() {
        DBI dbi = new DBI(new ConnectionFactory() {
            @Override
            public Connection openConnection() throws SQLException {
                return createConnection();
            }
        });
        dbi.registerArgumentFactory(new DateTimeAF());

        return dbi;
    }

}
