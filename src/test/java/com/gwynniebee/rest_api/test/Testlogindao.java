package com.gwynniebee.rest_api.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.loginresponseobject;
import com.gwynniebee.dao.EmpPerDetaildao;
import com.gwynniebee.dao.logindao;
import com.gwynniebee.rest_api.test.helper.LiquibaseOperations;
import com.gwynniebee.rest_api.test.helper.TestDAOBase;

public class Testlogindao extends TestDAOBase {

    public static final Logger LOG = LoggerFactory.getLogger(Testlogindao.class);

    @Test
    public void testloginDAO() {
        DBI dbi = LiquibaseOperations.getDBI();
        logindao l1 = dbi.open(logindao.class);
        EmpPerDetaildao e1 = dbi.open(EmpPerDetaildao.class);

        try {
            String uuid = "1234";
            String password = "sumit1";
            int reply =
                    e1.addPersoDetails("1234", "1991-04-06", "2014-07-01", "in1", "employed", "sumit", "gupta", "o+", "se",
                            "sgupta@gwynniebee.com");
            System.out.println(reply);
            loginresponseobject res = l1.authenticateuserandspecifyrole(uuid, password);
            System.out.println(res.role);
            assertEquals("employee", res.role);

        } finally {
            l1.close();
        }

    }

}
