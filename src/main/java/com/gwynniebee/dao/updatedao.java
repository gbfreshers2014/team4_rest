package com.gwynniebee.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.deleteresponseobject;
import com.gwynniebee.mappers.updateMapper;

@RegisterMapper(updateMapper.class)
public interface updatedao extends Transactional<updatedao>{
    
    //@SqlQuery("select UUID,role from Login where email_id=:emailid and password=:password")
    
	@SqlUpdate("Update employee_personal_details SET employement_status='retired' WHERE UUID=:UUID")
    
    int authenticateadminandupdaterecord(@Bind("UUID") String UUID);
//    int addNewData(@Bind("created_by") String created_by, @Bind("created_on") String created_on, @Bind("DOB") String DOB,
//    @Bind("DOJ") String DOJ, @Bind("employeeId") String employeeId, @Bind("employmentStatus") String employmentStatus,
//    @Bind("firstName") String firstName, @Bind("last_updated_by") String last_updated_by,
//    @Bind("last_updated_on") String last_updated_on, @Bind("lastName") String lastName, @Bind("UUID") String UUID);
}