package com.gwynniebee.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.loginresponseobject;
import com.gwynniebee.mappers.loginInternalMapper;

@RegisterMapper(loginInternalMapper.class)
public interface loginInternaldao extends Transactional<loginInternaldao>{
    @SqlQuery("select uuid from employee_personal_details where email_id=:email")
    String fetchuuid(@Bind("email")String email);
}
