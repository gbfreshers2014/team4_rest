package com.gwynniebee.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.loginresponseobject;
import com.gwynniebee.mappers.loginMapper;

@RegisterMapper(loginMapper.class)
public interface logindao extends Transactional<logindao> {

    @SqlQuery("select uuid,role from login_credentials where uuid=:uuid and password=:password")
    loginresponseobject authenticateuserandspecifyrole(@Bind("uuid") String uuid, @Bind("password") String password);

    void close();
}
