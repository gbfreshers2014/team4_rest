package com.gwynniebee.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.AddressDetails;
import com.gwynniebee.backoffice.objects.CommunicationDetails;
import com.gwynniebee.backoffice.objects.FamilyDetails;
import com.gwynniebee.backoffice.objects.PersonalDetails;
import com.gwynniebee.mappers.EmpAddressDetailsMapper;
import com.gwynniebee.mappers.EmpCommunicationDetailsMapper;
import com.gwynniebee.mappers.EmpFamilyDetailsMapper;
import com.gwynniebee.mappers.EmpPersonalDetailsMapper;

public interface EmpPerDetaildao extends Transactional<EmpPerDetaildao> {

    @RegisterMapper(EmpPersonalDetailsMapper.class)
    @SqlQuery("select * from employee_personal_details where uuid=:ud")
    PersonalDetails getPersonalData(@Bind("ud") String ud);

    @RegisterMapper(EmpFamilyDetailsMapper.class)
    @SqlQuery("select * from employee_family_details where uuid=:ud")
    List<FamilyDetails> getFamilyData(@Bind("ud") String ud);

    @RegisterMapper(EmpCommunicationDetailsMapper.class)
    @SqlQuery("select * from employee_communication_details where uuid=:ud")
    List<CommunicationDetails> getCommunicationData(@Bind("ud") String ud);

    @RegisterMapper(EmpAddressDetailsMapper.class)
    @SqlQuery("select * from employee_address_details where uuid=:ud")
    List<AddressDetails> getAddressData(@Bind("ud") String ud);

    // Update profiles starts here.........................................

    @SqlUpdate("UPDATE employee_communication_details SET details=:details WHERE uuid=:uuid and type=:type")
    int updateCommuDetails(@Bind("uuid") String uuid, @Bind("details") String details, @Bind("type") String type);

    @SqlUpdate("UPDATE employee_family_details SET blood_group=:bg, dependent=:dependent, dob=:dob, relation=:relation  WHERE uuid=:uuid and full_name=:fullName")
    int updateFamDetails(@Bind("uuid") String uuid, @Bind("bg") String bloodGroup, @Bind("dependent") String dependent,
            @Bind("dob") String dob, @Bind("fullName") String fullName, @Bind("relation") String relation);

    @SqlUpdate("UPDATE employee_address_details SET city=:city, country_code=:country,state=:state,street_address=:streetAdd, zip_code=:Zip  WHERE uuid=:uuid and type=:type")
    int updateAddressDetails(@Bind("uuid") String uuid, @Bind("city") String city, @Bind("country") String country,
            @Bind("state") String state, @Bind("streetAdd") String streetAddress, @Bind("type") String type, @Bind("Zip") String Zipcode);

    // @SqlUpdate("UPDATE employee_personal_details SET emp_id=:employeeId, first_name=:fisrtName, last_name=:lastName, dob=:DOB, doj=:DOJ, employement_status=:employmentStatus   WHERE uuid=:uuid")
    @SqlUpdate("UPDATE employee_personal_details SET designation=:designation, emp_blood_group=:bloodGroup, emp_id=:employeeId, first_name=:firstName, last_name=:lastName, dob=:DOB, doj=:DOJ, employement_status=:employmentStatus   WHERE uuid=:uuid")
    int updatePersoDetails(@Bind("uuid") String UUID, @Bind("DOB") String DOB, @Bind("DOJ") String DOJ,
            @Bind("employeeId") String employeeId, @Bind("employmentStatus") String employmentStatus, @Bind("firstName") String firstName,
            @Bind("lastName") String lastName, @Bind("bloodGroup") String BloodG, @Bind("designation") String designation);

    //

    // Add new employee details starts here.............

    @SqlUpdate("INSERT INTO employee_personal_details  (designation,emp_blood_group,emp_id,first_name,last_name,dob,doj,employement_status,uuid,email_id) VALUES (:designation, :bloodGroup, :employeeId, :firstName, :lastName, :DOB, :DOJ, :employmentStatus,:uuid,:email)")
    int addPersoDetails(@Bind("uuid") String UUID, @Bind("DOB") String DOB, @Bind("DOJ") String DOJ, @Bind("employeeId") String employeeId,
            @Bind("employmentStatus") String employmentStatus, @Bind("firstName") String firstName, @Bind("lastName") String lastName,
            @Bind("bloodGroup") String BloodG, @Bind("designation") String designation, @Bind("email") String email);

    @SqlUpdate("INSERT INTO employee_communication_details (details,uuid,type) VALUES (:details,:uuid,:type)")
    int addCommuDetails(@Bind("uuid") String uuid, @Bind("details") String details, @Bind("type") String type);

    @SqlUpdate("INSERT INTO employee_family_details ( blood_group,dependent,dob,relation,uuid, full_name) VALUES  (:bg,:dependent,:dob,:relation,:uuid ,:fullName)")
    int addFamDetails(@Bind("uuid") String uuid, @Bind("bg") String bloodGroup, @Bind("dependent") String dependent,
            @Bind("dob") String dob, @Bind("fullName") String fullName, @Bind("relation") String relation);

    @SqlUpdate("INSERT INTO employee_address_details (city,country_code,state,street_address,zip_code,uuid,type) VALUES (:city,:country,:state,:streetAdd,:Zip,:uuid,:type)")
    int addAddressDetails(@Bind("uuid") String uuid, @Bind("city") String city, @Bind("country") String country,
            @Bind("state") String state, @Bind("streetAdd") String streetAddress, @Bind("type") String type, @Bind("Zip") String Zipcode);

    //
    //
    // @SqlUpdate(":qury")
    // int updateTableValues(@Bind("qury") String qury);
    // @SqlUpdate("UPDATE employeePersonalDetails SET lastName=:lastName WHERE UUID=:uuid")
    // int updateTableValues(@Bind("lastName") String lastName, @Bind("uuid")
    // String uuid);
    //
    // @SqlUpdate("insert into employeePersonalDetails values (:UUID,:employeeId,:firstName,:lastName,:DOB,:DOJ,:employmentStatus,:created_on,:created_by,:last_updated_on,:last_updated_by)")
    // int addNewData(@Bind("created_by") String created_by, @Bind("created_on")
    // String created_on, @Bind("DOB") String DOB,
    // @Bind("DOJ") String DOJ, @Bind("employeeId") String employeeId,
    // @Bind("employmentStatus") String employmentStatus,
    // @Bind("firstName") String firstName, @Bind("last_updated_by") String
    // last_updated_by,
    // @Bind("last_updated_on") String last_updated_on, @Bind("lastName") String
    // lastName, @Bind("UUID") String UUID);

}
// @RegisterMapper(UpdateDetailsMapper.class)
//
