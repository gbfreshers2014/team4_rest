package com.gwynniebee.dao;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.backoffice.objects.deleteresponseobject;
import com.gwynniebee.mappers.deleteMapper;

@RegisterMapper(deleteMapper.class)
public interface deletedao extends Transactional<deletedao>{
    
    
	@SqlUpdate("Update employee_personal_details SET employement_status='retired' WHERE uuid=:UUID")
    
    int authenticateadminandchangeempstatus(@Bind("UUID") String UUID);
	
}