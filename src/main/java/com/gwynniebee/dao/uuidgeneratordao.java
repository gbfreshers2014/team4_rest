package com.gwynniebee.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.mixins.Transactional;

import com.gwynniebee.mappers.uuidgeneratorMapper;

@RegisterMapper(uuidgeneratorMapper.class)
public interface uuidgeneratordao extends Transactional<uuidgeneratordao>
{   
    @SqlQuery("select uuid from employee_personal_details where employement_status!='retired'")
    List<String> generateUUID();
}
