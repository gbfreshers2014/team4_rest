package com.gwynniebee.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.gwynniebee.backoffice.objects.personal_details;
import com.gwynniebee.backoffice.objects.searchResult;
import com.gwynniebee.backoffice.objects.searchResultArray;

public class searchMapper implements ResultSetMapper<searchResult>{

    @Override
    public searchResult map(int index, ResultSet r, StatementContext ctx) throws SQLException {
        
        searchResult sra=new searchResult();
        sra.personalDetails=new personal_details(r.getString("uuid"),r.getString("first_name"),r.getString("last_name"),r.getString("email_id"));
        sra.personalDetails.bloodGroup=r.getString("emp_blood_group");
        sra.personalDetails.designation=r.getString("designation");
        sra.personalDetails.employmentStatus=r.getString("employement_status");
        return sra;
    }
    
}
