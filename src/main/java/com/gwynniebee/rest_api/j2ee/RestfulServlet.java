/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.rest_api.j2ee;

import com.gwynniebee.rest.service.j2ee.GBRestletServlet;
import com.gwynniebee.rest_api.restlet.rest_app;

/**
 * @author sarath
 */
@SuppressWarnings("serial")
public class RestfulServlet extends GBRestletServlet<rest_app> {
    /**
     * This is called by tomcat's start of servlet.
     */
    public RestfulServlet() {
        this(new rest_app());
    }

    /**
     * This is called by unit test which create a subclass of this class with subclass of application.
     * @param app application
     */
    public RestfulServlet(rest_app app) {
        super(app);
    }
}
