/**
 * Copyright 2012 GwynnieBee Inc.
 */

package com.gwynniebee.rest_api.restlet.resources;

import org.restlet.resource.Get;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.barcode.upload.common.Constants;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;
import com.gwynniebee.rest_api.restlet.rest_app;

/**
 * resources class. To change this resource rename the class to the
 * required class.
 * @author sarath
 */
public class resources extends AbstractServerResource {

    private static final Logger LOG = LoggerFactory.getLogger(resources.class);

    /*
     * (non-Javadoc)
     * @see org.restlet.resource.UniformResource#doInit()
     */
    @Override
    protected void doInit() {
        super.doInit();
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailSubject()
     */
    @Override
    protected String getErrorEmailSubject() {
        return "Shipment Tracking Service - NetsuiteFileServerResource Error";
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailFrom()
     */
    @Override
    protected String getErrorEmailFrom() {
        return ((rest_app) this.getApplication()).getServiceProperties().getProperty(Constants.ALERT_EMAIL_FROM_PROP);
    }

    /*
     * (non-Javadoc)
     * @see com.gwynniebee.rest.service.restlet.resources.GBRestletResourse#
     * getErrorEmailTo()
     */
    @Override
    protected String getErrorEmailTo() {
        return ((rest_app) this.getApplication()).getServiceProperties().getProperty(Constants.ALERT_EMAIL_TO_PROP);
    }

    /**
     * javadoc.
     * @return email {String}
     */
    protected String getInfoEmailFrom() {
        return ((rest_app) this.getApplication()).getServiceProperties().getProperty(Constants.INFO_EMAIL_FROM_PROP);
    }

    /**
     * javadoc.
     * @return email {String}
     */
    protected String getInfoEmailTo() {
        return ((rest_app) this.getApplication()).getServiceProperties().getProperty(Constants.INFO_EMAIL_TO_PROP);
    }

    /**
     * Implementation of a GET method.
     * @return "hello world" {String}.
     */
    @Get
    public String sayHelloWorld() {
        LOG.debug("I am trying to say hello world");
        return "hello world";
    }
}
