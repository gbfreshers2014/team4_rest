package com.gwynniebee.backoffice.restlet.resources;

import java.sql.Date;
import java.util.ArrayList;

import org.apache.james.mime4j.field.datetime.DateTime;

import com.gwynniebee.backoffice.objects.leavesdetails;
import com.gwynniebee.rest.common.response.AbstractResponse;

public class leavesresponse extends AbstractResponse{
    public ArrayList<leavesdetails> leaves_details=new ArrayList<leavesdetails>();
    public int count;
}
