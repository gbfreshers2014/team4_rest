package com.gwynniebee.backoffice.restlet.resources;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class updateQuerybuilder {
    public String querybuilder(String table_name,Map<String,String> set_values,String where_value,String where_cloumnname) {
        StringBuilder query=new StringBuilder();
        query.append("update");
        query.append(" "+table_name);
        query.append(" ");
        query.append("set");
        Set<Entry<String, String>> sets=set_values.entrySet();
        for(Entry<String,String> it :sets)
        {
            query.append(" ");
            query.append(it.getKey()+"="+it.getValue());
            query.append(",");
        }
        query.append(" where");
     
            query.append(" ");
            query.append(where_value+"="+where_cloumnname);
        return query.toString();
    }
}
