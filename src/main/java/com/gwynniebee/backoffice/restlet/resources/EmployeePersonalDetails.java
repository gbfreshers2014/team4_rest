package com.gwynniebee.backoffice.restlet.resources;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.restlet.data.Status;
import org.restlet.resource.Get;
import org.restlet.resource.Put;
import org.slf4j.Logger;

import com.google.common.base.Strings;
import com.gwynniebee.backoffice.objects.StatusResponseObject;
import com.gwynniebee.entites.PersonalDetailsEntity;
import com.gwynniebee.entites.UpdateDetailsEntity;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

//import com.gwynniebee.rest.service.restlet.resources.;
public class EmployeePersonalDetails extends AbstractServerResource {

    public String tempUuid = "";
    Logger log = org.slf4j.LoggerFactory.getLogger(EmployeePersonalDetails.class);

    @Get
    public Details getEmployee() throws JsonParseException, IOException, ClassNotFoundException {

        Details response = new Details();
        tempUuid=(String)getRequest().getAttributes().get("UUID");
        // Get UUID here...............................
        // this.tempUuid = "12";
        // String uuid = this.tempUuid;

        // Process uuid from here.................
        // System.out.println(this.tempUuid + "dsad");
        // System.out.println(this.tempUuid);
        PersonalDetailsEntity temp = new PersonalDetailsEntity();
        response = temp.getPersonalInfo(this.tempUuid);
        //
        // System.out.println(response.firstName + response.UUID);
        //
        // // response.firstName = "rahul";
        // // response.UUID = this.tempUuid;
        return response;

    }

    @Put
    // public StatusResponseObject updateEmployee(Details obj) throws
    // JsonParseException, IOException, ClassNotFoundException
    public StatusResponseObject updateEmployee(Details obj) throws JsonParseException, IOException, ClassNotFoundException {
        StatusResponseObject response = new StatusResponseObject();
        obj.uuid = this.tempUuid;
        UpdateDetailsEntity temp = new UpdateDetailsEntity();
        response = temp.updateDetails(obj);

        return response;
        // return obj;
    }

    // @Post
    // public PersonalDetails a() {
    // }

}
