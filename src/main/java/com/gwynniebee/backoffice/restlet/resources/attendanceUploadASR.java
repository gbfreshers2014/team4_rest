package com.gwynniebee.backoffice.restlet.resources;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.lang.text.StrBuilder;
import org.joda.time.DateTime;
import org.restlet.ext.fileupload.RestletFileUpload;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.FileDetails;
import com.gwynniebee.barcode.upload.common.Constants;
import com.gwynniebee.cloudio.CloudAccessClient;
import com.gwynniebee.cloudio.CloudAccessClientFactory;
import com.gwynniebee.cloudio.Entry;
import com.gwynniebee.cloudio.SaveOptions;
import com.gwynniebee.entites.attendanceEntityManager;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

public class attendanceUploadASR extends AbstractServerResource{
    byte[] uploadBytes;
    public static final String CONST_UTF_8 = "UTF-8";
    private static final Logger LOG = LoggerFactory.getLogger(loginASR.class);
    @Post("multipart")
    public ResponseStatus uploadAttendance(Representation entity) throws Exception
    {
        LOG.debug("HERE AT ATTENDNACE UPLOAD");
        
        LOG.debug("HERE AT FILE LOAD");
        Map<String,String> rv=fileuploader();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date tempDate=sdf1.parse(rv.get("date"));
        java.sql.Date date = new java.sql.Date(tempDate.getTime()); 
        LOG.debug("HERE RV"+rv.get("action"));
        if(rv.get("action").equals("uploadcsv"))
        {
            LOG.debug("HERE AT UPLOADc"+tempDate);
            CloudAccessClient s3client = CloudAccessClientFactory.getS3CloudAccessClient("_dev_store");
            Entry entry=s3client.createNewEntry("gbfreshers", 0, "attendance_"+tempDate);
            SaveOptions saveOption = new SaveOptions().withPublicAccess(true);
            LOG.debug("HERE AT savingUPLOAD");
            s3client.save(entry,".csv",rv.get("attendance"),saveOption);
            LOG.debug("HERE AT savedUPLOAD");
            String str=stringbuilded();
            HashSet<String> absent=new HashSet<String>();
            for (String it : str.split(","))
            {
                LOG.debug(it.trim());
                absent.add(it.trim());
            }
    
            LOG.debug("HERE AT WILL DO UPLOAD");
            attendanceEntityManager aer=new attendanceEntityManager();
            aer.updateAttendance(absent,date);
        }
        else
        {
            attendanceEntityManager aer=new attendanceEntityManager();
            aer.updateAttendance(date,rv.get("action"));
        }
        return getRespStatus();
   }
   Map<String, String> fileuploader() throws Exception
    {
        Map<String,String> rv=new HashMap<String,String>();
        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(1000240);
        RestletFileUpload upload = new RestletFileUpload(factory);
        List<FileItem> items=upload.parseRepresentation(getRequestEntity());
//        items = upload.parseRequest(this.getRequest());
        uploadBytes = null;
        if (items != null) {
            for (FileItem item : items) {
                String fiFieldName = item.getFieldName();
               
                if (item.getFieldName().equals("file")){
                    uploadBytes = item.get();
                    rv.put("attendance", new String(uploadBytes));
                    LOG.debug(new String(uploadBytes));
                }
                else
                {
                    String getValue = new String(item.get(), CONST_UTF_8);
                    rv.put(fiFieldName, getValue);
                }
            }
        }
        else {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.append("Invalid Media Type: ");
                sbMessage.append(this.getRequestEntity().getMediaType());
                throw new Exception(sbMessage.toString());
              }
        
        
        return rv;
    }
   String stringbuilded()
   {

       String str=new String(uploadBytes);
       LOG.debug(str);
       return str;
       /*
       StringBuilder strbuild=new StringBuilder();
       
       for(byte b : )
       {
           Byte b_b=b;
           b_b.toString();
           LOG.debug(String.valueOf(b));
           LOG.debug("-"+String.valueOf(b_b));
           strbuild.append(String.valueOf(b));
       }
       return strbuild.toString();
       */
   }
}