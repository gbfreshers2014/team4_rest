package com.gwynniebee.backoffice.restlet.resources;

import java.sql.SQLException;

import liquibase.exception.LiquibaseException;

import org.restlet.resource.Get;

import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

public class liquibaseASR extends AbstractServerResource{
    @Get
    public liquibaseHandler synchronizeDB() throws SQLException, LiquibaseException
    {
        ResponseStatus rs=new ResponseStatus();
        liquibaseHandler lh=new liquibaseHandler();
        lh.synchronizeDB();
        rs.setCode(200);
        rs.setMessage("SuccessFul");
        lh.setStatus(rs);
        return null;
    }
}