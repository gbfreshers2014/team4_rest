package com.gwynniebee.backoffice.restlet.resources;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.restlet.resource.Delete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.deleteresponseobject;
import com.gwynniebee.entites.deleteEntity;
import com.gwynniebee.rest.common.response.ResponseStatus;
import com.gwynniebee.rest.service.restlet.resources.AbstractServerResource;

public class deleteASR extends AbstractServerResource {
    private static final Logger LOG = LoggerFactory.getLogger(searchASR.class);

    public String tempUuid = "";

    @Override
    protected void doInit() {
        super.doInit();
        this.tempUuid = (String) this.getRequestAttributes().get("uuid");
    }

    @Delete
    public deletehandler handle_delete() throws JsonParseException, IOException, ClassNotFoundException {
        ResponseStatus status = new ResponseStatus();
        deleteEntity deleteentityinstance = new deleteEntity();

        deletehandler deletehandlerinstance = new deletehandler();
        deleteresponseobject lro = deleteentityinstance.authenticate(this.tempUuid);
        LOG.debug("done authentication");
        if (lro == null) {
            // deletehandlerinstance.msg = "error";
            status.setCode(ResponseStatus.ERR_CODE_INVALID_REQ);
            status.setMessage(ResponseStatus.MESSAGE_RESOURCE_NOT_FOUND);
            deletehandlerinstance.setStatus(status);
        } else {
            // deletehandlerinstance.msg = lro.msg;
            status.setCode(lro.code);
            status.setMessage(lro.msg);
            deletehandlerinstance.setStatus(status);
        }
        return deletehandlerinstance;
    }
}
