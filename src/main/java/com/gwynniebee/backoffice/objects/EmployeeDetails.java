package com.gwynniebee.backoffice.objects;

import java.util.ArrayList;
import java.util.List;

public class EmployeeDetails {

    PersonalDetails personalDetails = new PersonalDetails();
    List<AddressDetails> addressDetails = new ArrayList<AddressDetails>();
    List<CommunicationDetails> communicationDetails = new ArrayList<CommunicationDetails>();
    List<FamilyDetails> familyDetails = new ArrayList<FamilyDetails>();

    public PersonalDetails getPersonalDetails() {
        return this.personalDetails;
    }

    public void setPersonalDetails(PersonalDetails personalDetails) {
        this.personalDetails = personalDetails;
    }

    public List<AddressDetails> getAddressDetails() {
        return this.addressDetails;
    }

    public void setAddressDetails(List<AddressDetails> addressDetails) {
        this.addressDetails = addressDetails;
    }

    public List<CommunicationDetails> getCommunicationDetails() {
        return this.communicationDetails;
    }

    public void setCommunicationDetails(List<CommunicationDetails> communicationDetails) {
        this.communicationDetails = communicationDetails;
    }

    public List<FamilyDetails> getFamilyDetails() {
        return this.familyDetails;
    }

    public void setFamilyDetails(List<FamilyDetails> familyDetails) {
        this.familyDetails = familyDetails;
    }

}
