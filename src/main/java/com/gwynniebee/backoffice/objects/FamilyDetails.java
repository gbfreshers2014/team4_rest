package com.gwynniebee.backoffice.objects;

public class FamilyDetails {

    public String fullName = "";
    public String relation = "";
    public String dependent = "";
    public String dob = "";
    public String bloodGroup = "";

}
