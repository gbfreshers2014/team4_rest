package com.gwynniebee.entites;

import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.loginObject;
import com.gwynniebee.backoffice.objects.loginresponseobject;
import com.gwynniebee.dao.loginInternaldao;
import com.gwynniebee.dao.logindao;

public class loginEntityManager {
    private static final Logger LOG = LoggerFactory.getLogger(loginEntityManager.class);

    public loginresponseobject authenticate(loginObject login_inst) throws ClassNotFoundException {
        Handle h = BaseEntityManager.getDBIh();
        logindao dao = null;
        loginInternaldao idao = null;
        h.begin();
        idao = h.attach(loginInternaldao.class);
        dao = h.attach(logindao.class);
        String uuid = idao.fetchuuid(login_inst.emailid);
        LOG.debug("UUID:" + uuid);
        loginresponseobject tobereturned = dao.authenticateuserandspecifyrole(uuid, login_inst.password);
        LOG.debug("Here before role and uuid fetch");
        // LOG.debug(tobereturned.role + tobereturned.uuid);
        h.commit();
        h.close();
        return tobereturned;
    }
}
