package com.gwynniebee.entites;

import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.deleteresponseobject;
import com.gwynniebee.backoffice.restlet.resources.searchASR;
import com.gwynniebee.dao.deletedao;

public class deleteEntity {
    private static final Logger LOG = LoggerFactory.getLogger(searchASR.class);

    public deleteresponseobject authenticate(String temp) throws ClassNotFoundException {
        Handle h = BaseEntityManager.getDBIh();
        deletedao dao = null;
        h.begin();
        LOG.debug("Here at delete authenticate");
        dao = h.attach(deletedao.class);
        deleteresponseobject tobereturned = new deleteresponseobject();
        LOG.debug("Calling SQL query");
        LOG.debug("uuid = " + temp);
        int returncode = dao.authenticateadminandchangeempstatus(temp);
        if (returncode != 0) {
            tobereturned.msg = "Success";
            tobereturned.code = 0;
        } else {
            tobereturned.msg = "Failure";
            tobereturned.code = 1;
        }
        h.commit();
        h.close();
        return tobereturned;
    }

}
