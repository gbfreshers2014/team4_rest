package com.gwynniebee.entites;

import java.util.HashMap;

import org.skife.jdbi.v2.Handle;

import com.gwynniebee.backoffice.restlet.resources.updateQuerybuilder;
import com.gwynniebee.mappers.updateMapper;

public class updateEntity {
    public void updateDetail(String table_name, HashMap<String, String> set_values, HashMap<String, String> where_values,
            String where_columnname, String where_value) throws ClassNotFoundException {
        Handle h = BaseEntityManager.getDBIh();
        updateQuerybuilder uqb = new updateQuerybuilder();
        String sql = uqb.querybuilder(table_name, set_values, where_value, where_columnname);
        h.update(sql, updateMapper.class);
        h.commit();
        h.close();
    }
}
