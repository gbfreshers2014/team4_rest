package com.gwynniebee.entites;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

import com.gwynniebee.backoffice.objects.AddressDetails;
import com.gwynniebee.backoffice.objects.CommunicationDetails;
import com.gwynniebee.backoffice.objects.EmployeeDetails;
import com.gwynniebee.backoffice.objects.FamilyDetails;
import com.gwynniebee.backoffice.objects.PersonalDetails;
import com.gwynniebee.backoffice.objects.StatusResponseObject;
import com.gwynniebee.backoffice.restlet.resources.Details;
import com.gwynniebee.dao.EmpPerDetaildao;

//import com.gwynniebee.dao.logindao;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddEmpEntity {

    public StatusResponseObject addEmployee(Details obj) throws ClassNotFoundException {
        Handle h = BaseEntityManager.getDBIh();
        EmpPerDetaildao dao = null;
        h.begin();
        dao = h.attach(EmpPerDetaildao.class);
        // loginresponseobject tobereturned =
        //
        StatusResponseObject toBeReturned = new StatusResponseObject();
        int output = 0;
        EmployeeDetails temp = new EmployeeDetails();
        temp = obj.getEmployeeDetails();
        PersonalDetails perTemp = new PersonalDetails();
        perTemp = temp.getPersonalDetails();
        List<CommunicationDetails> commuTemp = new ArrayList<CommunicationDetails>();
        commuTemp = temp.getCommunicationDetails();
        List<FamilyDetails> famTemp = new ArrayList<FamilyDetails>();
        famTemp = temp.getFamilyDetails();
        List<AddressDetails> addressTemp = new ArrayList<AddressDetails>();
        addressTemp = temp.getAddressDetails();

        int output1 = 0;
        int output2 = 0;
        int output3 = 0;
        int output4 = 0;
        output1 =
                dao.addPersoDetails(obj.uuid, perTemp.dob, perTemp.doj, perTemp.employeeId, perTemp.employmentStatus, perTemp.firstName,
                        perTemp.lastName, perTemp.bloodGroup, perTemp.designation, perTemp.email);

        for (CommunicationDetails tempObj : commuTemp) {
            output2 = dao.addCommuDetails(obj.uuid, tempObj.details, tempObj.type);

        }

        for (FamilyDetails tempObj : famTemp) {
            output3 = dao.addFamDetails(obj.uuid, tempObj.bloodGroup, tempObj.dependent, tempObj.dob, tempObj.fullName, tempObj.relation);

        }

        for (AddressDetails tempObj : addressTemp) {
            output4 =
                    dao.addAddressDetails(obj.uuid, tempObj.city, tempObj.country, tempObj.state, tempObj.streetAddress, tempObj.type,
                            tempObj.Zipcode);

        }

        if ((output3 != 0) && (output4 != 0) && (output1 != 0) && (output2 != 0)) {
            toBeReturned.status = 0;
            toBeReturned.message = "updated_successfully";

        } else {
            toBeReturned.status = 1;
            toBeReturned.message = "not_updated";
        }

        h.commit();
        h.close();
        // System.out.println(toBeReturned.employeeId);
        return toBeReturned;
    }
}
