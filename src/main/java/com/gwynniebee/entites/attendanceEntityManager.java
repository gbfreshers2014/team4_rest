package com.gwynniebee.entites;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;

import org.skife.jdbi.v2.Handle;

import com.gwynniebee.dao.attendanceuploaddao;
import com.gwynniebee.dao.searchdao;
import com.gwynniebee.dao.uuidgeneratordao;

public class attendanceEntityManager {
    
    
    public int updateAttendance(HashSet<String> uuid_set,Date date) throws Exception
    {
    Handle h=BaseEntityManager.getDBIh();
    uuidgeneratordao dao=h.attach(uuidgeneratordao.class);
    attendanceuploaddao atddao=h.attach(attendanceuploaddao.class);
    h.begin();
    ArrayList<String> uuidlist=(ArrayList<String>) dao.generateUUID();
    for(String uuid:uuidlist)
    {
        if(uuid_set.contains(uuid))
        {
            atddao.updateattendance(uuid, "absent", date);
        }
        else
            atddao.updateattendance(uuid, "present", date);
    }
    h.commit();
    h.close();
    return 0;
    }

    public int updateAttendance( java.sql.Date date, String string) throws ClassNotFoundException {
        Handle h=BaseEntityManager.getDBIh();
        uuidgeneratordao dao=h.attach(uuidgeneratordao.class);
        attendanceuploaddao atddao=h.attach(attendanceuploaddao.class);
        h.begin();
        ArrayList<String> uuidlist=(ArrayList<String>) dao.generateUUID();
       
        for(String uuid:uuidlist)
        {
            if(string.equals("allpresent"))
                atddao.updateattendance(uuid, "present", date);
            if(string.equals("holiday"))
                    atddao.updateattendance(uuid, "holiday", date);
        }
        h.commit();
        h.close();
        return 0;
    }
}