package com.gwynniebee.entites;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.backoffice.objects.EmployeeDetails;
import com.gwynniebee.backoffice.restlet.resources.Details;
import com.gwynniebee.backoffice.restlet.resources.loginASR;
import com.gwynniebee.dao.EmpPerDetaildao;

//import com.gwynniebee.dao.logindao;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonalDetailsEntity {
    private static final Logger LOG = LoggerFactory.getLogger(PersonalDetailsEntity.class);

    // public static void main(String[] a) throws JsonParseException,
    // ClassNotFoundException, IOException {
    // PersonalDetailsEntity aaaa = new PersonalDetailsEntity();
    // aaaa.getPersonalInfo("1");
    //
    // }

    public Details getPersonalInfo(String uuid) throws ClassNotFoundException {
        Handle h = BaseEntityManager.getDBIh();
        EmpPerDetaildao dao = null;
        LOG.debug("UUID:"+uuid);
        h.begin();
        dao = h.attach(EmpPerDetaildao.class);
        // loginresponseobject tobereturned =
        //
        Details toBeReturned = new Details();

        // toBeReturned = dao.getTableValues(uuid);

        // uuid = "1235";
        EmployeeDetails tempEmpDetail = new EmployeeDetails();
        // tempEmpDetail.getPersonalDetails() = dao.getPersonalData(uuid);
        tempEmpDetail.setPersonalDetails(dao.getPersonalData(uuid));
        LOG.debug("UUID:"+tempEmpDetail.getPersonalDetails());
        tempEmpDetail.setAddressDetails(dao.getAddressData(uuid));
        LOG.debug("UUID:"+tempEmpDetail.getAddressDetails());
        tempEmpDetail.setFamilyDetails(dao.getFamilyData(uuid));
        LOG.debug("UUID:"+tempEmpDetail.getFamilyDetails());
        tempEmpDetail.setCommunicationDetails(dao.getCommunicationData(uuid));
        LOG.debug("UUID:"+tempEmpDetail.getCommunicationDetails());
        // dao.getCommunicationData(uuid);

        // System.out.println(tempEmpDetail.getFamilyDetails());
        // System.out.println(tempEmpDetail.getCommunicationDetails());
        toBeReturned.setEmployeeDetails(tempEmpDetail);

        // PersonalDetails personalData = dao.getPersonalData(uuid);

        // toBeReturned = dao.getTableValues("11");
        // toBeReturned.employeeId = "sda";
        // toBeReturned.firstName = "rahul sharmaaa";

        h.commit();
        h.close();
        // System.out.println(toBeReturned.employeeId);
        return toBeReturned;
    }
}
