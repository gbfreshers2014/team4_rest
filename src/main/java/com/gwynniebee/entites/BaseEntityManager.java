package com.gwynniebee.entites;

import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

public class BaseEntityManager {

    
    public static Handle getDBIh() throws ClassNotFoundException {
        Handle h;
        Class.forName("com.mysql.jdbc.Driver");
        DBI dbi=new DBI("jdbc:mysql://induction-dev.gwynniebee.com/t4","write_all_bi","write_all_bi");
       // DBI dbi=new DBI("jdbc:mysql://localhost/Employee_Management","root","root");
        h=dbi.open();
        return h;
    }

}
