package com.gwynniebee.entites;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

import com.gwynniebee.backoffice.objects.AddressDetails;
import com.gwynniebee.backoffice.objects.CommunicationDetails;
import com.gwynniebee.backoffice.objects.EmployeeDetails;
import com.gwynniebee.backoffice.objects.FamilyDetails;
import com.gwynniebee.backoffice.objects.PersonalDetails;
import com.gwynniebee.backoffice.objects.StatusResponseObject;
import com.gwynniebee.backoffice.restlet.resources.Details;
import com.gwynniebee.dao.EmpPerDetaildao;

//import com.gwynniebee.dao.logindao;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateDetailsEntity {

    // public static void main(String[] a) throws ClassNotFoundException {
    // UpdateDetailsEntity aaa = new UpdateDetailsEntity();
    // UpdateDetailsEntity obj = new UpdateDetailsEntity();
    // Details objj = new Details();
    // aaa.updateDetails(objj);
    //
    // }

    public StatusResponseObject updateDetails(Details obj) throws ClassNotFoundException {
        Handle h = BaseEntityManager.getDBIh();
        EmpPerDetaildao dao = null;
       
        h.begin();
        dao = h.attach(EmpPerDetaildao.class);
        // // loginresponseobject tobereturned =

        StatusResponseObject toBeReturned = new StatusResponseObject();

        int flag = 0;
        String query = "UPDATE employeePersonalDetails SET";
        // if (obj.created_by.length() >= 1) {
        // query = query + " " + "created_by=\"" + obj.created_by + "\"";
        // flag = 1;
        // }
        // if (obj.created_on.length() >= 1) {
        // if (flag == 1) {
        // query = query + "," + "created_on=" + obj.created_on;
        // } else {
        // query = query + " " + "created_on=" + obj.created_on;
        // }
        // flag = 1;
        // }
        // if (obj.DOB.length() >= 1) {
        // if (flag == 1) {
        // query = query + "," + "DOB=" + obj.DOB;
        // } else {
        // query = query + " " + "DOB=" + obj.DOB;
        // }
        // flag = 1;
        // }
        //
        // if (obj.DOJ.length() >= 1) {
        // if (flag == 1) {
        // query = query + "," + "DOJ=" + obj.DOJ;
        // } else {
        // query = query + " " + "DOJ=" + obj.DOJ;
        // }
        // flag = 1;
        // }
        //
        // if (obj.employeeId.length() >= 1) {
        // if (flag == 1) {
        // query = query + "," + "employeeId=" + obj.employeeId;
        // } else {
        // query = query + " " + "employeeId=" + obj.employeeId;
        // }
        // flag = 1;
        // }
        //
        // if (obj.employeeStatus.length() >= 1) {
        // if (flag == 1) {
        // query = query + "," + "employeeStatus=" + obj.employeeStatus;
        // } else {
        // query = query + " " + "employeeStatus=" + obj.employeeStatus;
        // }
        // flag = 1;
        // }

        // if (obj.lastName.length() >= 1) {
        // if (flag == 1) {
        // query = query + "," + "lastName=" + obj.lastName;
        // } else {
        // query = query + " " + "lastName=" + obj.lastName;
        // }
        // flag = 1;
        // }
        // if (flag == 1) {
        // query = query + " WHERE UUID=" + obj.UUID;
        // }

        int output = 0;
        EmployeeDetails temp = new EmployeeDetails();

        temp = obj.getEmployeeDetails();
        PersonalDetails perTemp = new PersonalDetails();
        perTemp = temp.getPersonalDetails();

        List<CommunicationDetails> commuTemp = new ArrayList<CommunicationDetails>();
        commuTemp = temp.getCommunicationDetails();

        List<FamilyDetails> famTemp = new ArrayList<FamilyDetails>();
        famTemp = temp.getFamilyDetails();

        List<AddressDetails> addressTemp = new ArrayList<AddressDetails>();
        addressTemp = temp.getAddressDetails();

        int output1 = 0;
        int output2 = 0;
        int output3 = 0;
        int output4 = 0;
        output1 =
                dao.updatePersoDetails(obj.uuid, perTemp.dob, perTemp.doj, perTemp.employeeId, perTemp.employmentStatus, perTemp.firstName,
                        perTemp.lastName, perTemp.bloodGroup, perTemp.designation);

        for (CommunicationDetails tempObj : commuTemp) {
            output2 = dao.updateCommuDetails(obj.uuid, tempObj.details, tempObj.type);

        }

        for (FamilyDetails tempObj : famTemp) {
            output3 =
                    dao.updateFamDetails(obj.uuid, tempObj.bloodGroup, tempObj.dependent, tempObj.dob, tempObj.fullName, tempObj.relation);

        }

        for (AddressDetails tempObj : addressTemp) {
            output4 =
                    dao.updateAddressDetails(obj.uuid, tempObj.city, tempObj.country, tempObj.state, tempObj.streetAddress, tempObj.type,
                            tempObj.Zipcode);

        }

        // if (flag != 0) {
        // String
        // qury="UPDATE employeePersonalDetails SET lastName=bain WHERE UUID=2";
        // output = dao.updateTableValues();
        // output = dao.updateTableValues(qury);
        // System.out.println(output + "raa");
        // toBeReturned = dao.getTableValues("11");
        // toBeReturned.employeeId = "sda";
        // toBeReturned.firstName = "rahul sharmaaa";
        // }

        if ((output3 != 0) && (output4 != 0) && (output1 != 0) && (output2 != 0)) {
            toBeReturned.status = 0;
            toBeReturned.message = "updated_successfully";

        } else {
            toBeReturned.status = 1;
            toBeReturned.message = "not_updated";
        }
        h.commit();
        h.close();
        // System.out.println(toBeReturned.employeeId);
        return toBeReturned;
    }
}
